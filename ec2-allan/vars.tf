variable "ENV" {
  type        = string
  default     = "teste-do-allan"
  description = " variavel de ambiente"
}


variable "RANGE_IPS_SR" {
  type        = list
  default     = ["IP/32"]
  description = "LISTA DE IPS PARA ACESSO AO CLUSTER"
}

variable "AWS_REGION" {
  type        = string
  default     = "us-east-1"
  description = " zoda de disponibilidade da aws"
}

variable services_ports {
  type        = list
  default     = ["80","22"]
  description = "Portas de aplicação e ancesso"
}



variable "VPCNAME" {
  type        = string
  default     = "vpc-id"
  description = "id da vpc onde o cluster seŕa iniciado."
}


variable "AWS_AMI" {
  type        = string
  default     = "ami-id"
  description = "ami cluster."
}



variable "AWS_TYPE_INSTANCE" {
  type        = string
  default     = "t2.medium"
  description = "tipo de instancia."
}



variable "KEYEC2" {
  type        = string
  default     = "Key-name"
  description = "chave de acesso as maquinas."
}



variable "SUBNET_EC2" {
  type        = string
  default     = "subnet-id"
  description = "id da subnet."
}



variable "SIZE_VOLUME" {
  type        = string
  default     = 30
  description = "tamanho do volume."
}



variable "TYPE_VOLUME" {
  type        = string
  default     = "gp2"
  description = "propísito do volume na aws."
}

