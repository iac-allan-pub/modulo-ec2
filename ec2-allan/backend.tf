terraform {
  backend "s3" {
    bucket  = "S3-name"
    encrypt = true
    key     = "vortx-prod/app-nginx"
    region  = "us-east-1"
  }
}