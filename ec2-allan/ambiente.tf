module teste-do-allan {
  source = "../modules/ec2-allan/"
 ENV = "${var.ENV}"
 RANGE_IPS_SR   = "${var.RANGE_IPS_SR}"
 AWS_REGION   = "${var.AWS_REGION}"
 services_ports   = "${var.services_ports}"
 VPCNAME   = "${var.VPCNAME}"
 AWS_AMI   = "${var.AWS_AMI}"
 AWS_TYPE_INSTANCE   = "${var.AWS_TYPE_INSTANCE}"
 KEYEC2    = "${var.KEYEC2}"
 SUBNET_EC2   = "${var.SUBNET_EC2}"
 SIZE_VOLUME   = "${var.SIZE_VOLUME}"
 TYPE_VOLUME   = "${var.TYPE_VOLUME}"
}