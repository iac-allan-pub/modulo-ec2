terraform {
  backend "s3" {
    bucket  = "ifnromar-o-s3-da-conta/"
    encrypt = true
    key     = "vortx-prod/app-prod"
    region  = "us-east-1"
  }
}